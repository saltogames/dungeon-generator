import './App.css';
import {Screen} from '../screen/Screen'
import { generateDungeon } from '../logics/generator';
import { useEffect, useState } from 'react';

export const App = () => {
  const initState = {
    dungeonData: {},
    coreCountInputValue: 15,
    coreCount: 15,
    deadEndInputValue: 0.04,
    deadEndChance : 0.04,    
    intersectionChanceInputValue: 0.03,
    intersectionChance : 0.03,
    roomChanceInputValue: 0.1,
    roomChance: 0.1,
    turnChanceInputValue: 0.08,
    turnChance : 0.08,
  }  
  const [state, setState] = useState(initState);
  const {
    dungeonData,
    coreCountInputValue,
    coreCount,
    intersectionChanceInputValue,
    intersectionChance,    
    deadEndInputValue,
    deadEndChance,
    roomChanceInputValue,
    roomChance,    
    turnChanceInputValue,
    turnChance,
  } = state;

  const onGenerateClicked = () => {  
    setState({
      ...state,
      coreCount: coreCountInputValue,
      intersectionChance: intersectionChanceInputValue,
      turnChance: turnChanceInputValue,
      deadEndChance: deadEndInputValue,
      roomChance: roomChanceInputValue,
      dungeonData: generateDungeon(130, 60, coreCount, intersectionChance, turnChance, deadEndChance, roomChance)
    })
  }

  useEffect(() => {
    let initialDungeonData = generateDungeon(130, 60, coreCount,  intersectionChance, turnChance, deadEndChance, roomChance)
    setState({ ...state, dungeonData: initialDungeonData})
  }, []);

  return (
    <div className="app">
      <div className="controls">
        <div className="title">Simple Dungeon Generator</div>
        <div>
          Core Count:
          <input onChange={e => {
              setState({...state, coreCountInputValue: parseFloat(e.target.value)})
          }} defaultValue={coreCount}/>
        </div>
        <div>
          Intersecion Chance:
          <input onChange={e => {
              setState({...state, intersectionChanceInputValue: parseFloat(e.target.value)})
          }} defaultValue={intersectionChance}/>
        </div>
        <div>
          Turn Chance:
          <input onChange={e => {
              setState({...state, turnChanceInputValue: parseFloat(e.target.value)})
          }} defaultValue={turnChance}/>
        </div>
        <div>
          Dead-end Chance:
          <input onChange={e => {
              setState({...state, deadEndInputValue: parseFloat(e.target.value)})
          }} defaultValue={deadEndChance}/>
        </div>
        <div>
          Room Chance:
          <input onChange={e => {
              setState({...state, roomChanceInputValue: parseFloat(e.target.value)})
          }} defaultValue={roomChance}/>
        </div>
        <div>
          <button onClick={onGenerateClicked}>GENERATE</button>
        </div>
      </div>      
      <Screen dungeonTiles={dungeonData} tileSize={8}/>
    </div>
  )
}  
