const getRandomInt = (max) => Math.floor(Math.random(1)*max)

const getOpenEnds = tiles => tiles.filter(tile =>         
    tile.topOpen ||
    tile.bottomOpen ||
    tile.leftOpen ||
    tile.rightOpen        
)  

const getRandomOpenEndedTile = tiles => {
    const openEnds = getOpenEnds(tiles)
    if (openEnds.length === 0 ) return undefined;
    return openEnds[getRandomInt(openEnds.length)];
}

const getRandomOpenDirectionOfTile = tile => {
    const openDirections = [];
    if (tile.topOpen) {
        openDirections.push({x: 0, y: -1, txt: 'top'})
    }
    if (tile.bottomOpen) {
        openDirections.push({x: 0, y: 1, txt: 'bottom'})        
    }
    if (tile.leftOpen) {
        openDirections.push({x: -1, y: 0, txt: 'left'})
    }
    if (tile.rightOpen) {
        openDirections.push({x: 1, y: 0, txt: 'right'})        
    }
    if (openDirections.length === 0) return undefined
    else return openDirections[getRandomInt(openDirections.length)]
}

const getRandomGrowingAction = (intersectionChance, turnChance, deadEndChance, roomChance) => {
    let r = Math.random(1);
    let r2 = Math.random(1);
    if (r < intersectionChance) {
        if (r2 < 0.5) return 'intersect-left'
        else return 'intersect-right'
    }
    else if (intersectionChance < r && r < (intersectionChance + turnChance)) {
        if (r2 < 0.5) return 'turn-left'
        else return 'turn-right'
    } else if (intersectionChance + turnChance < r && r < intersectionChance + turnChance + deadEndChance) {
        return 'dead-end'
    } else if (intersectionChance + turnChance + deadEndChance < r && r < intersectionChance + turnChance + deadEndChance + roomChance) {
        return 'room'
    }
    else return 'nothing'
}

const getGrowingAction = (fromTile, direction, intersectionChance, turnChance, deadEndChance, roomChance) => {    
    if (
        fromTile.type === 'corridor' &&
        (fromTile.x + direction.x) % 2 === 0 &&
        (fromTile.y + direction.y) % 2 === 0
    ){      
        return getRandomGrowingAction(intersectionChance, turnChance, deadEndChance, roomChance);  
    } else {
        return 'nothing';
    }    
}

const createNewTileIfAble = (tiles, maxWidth, maxHeight, fromTile, direction, growingAction) => {
    if (!fromTile || !direction) return;
    if (fromTile.x + direction.x > maxWidth) {        
        fromTile.rightOpen = false;
    } else if (fromTile.x + direction.x < 0) {        
        fromTile.leftOpen = false;
    } else if (fromTile.y + direction.y > maxHeight) {        
        fromTile.bottomOpen = false;
    } else if (fromTile.y + direction.y < 0) {        
        fromTile.topOpen = false;
    } else {    
        const newTile = {
            x: fromTile.x + direction.x,
            y: fromTile.y + direction.y,
            type: 
                growingAction === 'intersect-left' || growingAction === 'intersect-right' ? 'intersection' :
                growingAction === 'turn-left' || growingAction === 'turn-right' ? 'turn' :
                growingAction === 'dead-end' ? 'dead-end' :
                'corridor'                
        }        
        if (direction.txt === 'top') {
            fromTile.topOpen = false;
            newTile.topOpen = true;
            if (growingAction === 'intersect-right') newTile.rightOpen = true;
            if (growingAction === 'intersect-left') newTile.leftOpen = true;
            if (growingAction === 'turn-right') {newTile.rightOpen = true; newTile.topOpen = false;}
            if (growingAction === 'turn-left') {newTile.leftOpen = true; newTile.topOpen = false; }
        }
        if (direction.txt === 'bottom') {
            fromTile.bottomOpen = false;
            newTile.bottomOpen = true;
            if (growingAction === 'intersect-right') newTile.leftOpen = true;
            if (growingAction === 'intersect-left') newTile.rightOpen = true;
            if (growingAction === 'turn-right') {newTile.leftOpen = true; newTile.bottomOpen = false; newTile.bottomOpen = false;}
            if (growingAction === 'turn-left') {newTile.rightOpen = true; newTile.bottomOpen = false; newTile.bottomOpen = false;}
        }
        if (direction.txt === 'left') {
            fromTile.leftOpen = false;
            newTile.leftOpen = true;
            if (growingAction === 'intersect-right') newTile.topOpen = true;
            if (growingAction === 'intersect-left') newTile.bottomOpen = true;
            if (growingAction === 'turn-right') {newTile.topOpen = true; newTile.leftOpen = false;}
            if (growingAction === 'turn-left') {newTile.bottomOpen = true; newTile.leftOpen = false;}
            
        }
        if (direction.txt === 'right') {
            fromTile.rightOpen = false;
            newTile.rightOpen = true;
            if (growingAction === 'intersect-right') newTile.bottomOpen = true;
            if (growingAction === 'intersect-left') newTile.topOpen = true;
            if (growingAction === 'turn-right') {newTile.bottomOpen = true; newTile.rightOpen = false;}
            if (growingAction === 'turn-left') {newTile.topOpen = true; newTile.rightOpen = false;}
        }
        if (growingAction === 'dead-end') {
            newTile.topOpen = false;
            newTile.bottomOpen = false;
            newTile.leftOpen = false;
            newTile.rightOpen = false;
        }
        const canPlaceToTheNewPlace = tiles.filter(tile => tile.x === newTile.x && tile.y === newTile.y).length === 0           
        if (canPlaceToTheNewPlace){
            tiles.push(newTile)             
        }
    }
}
const createNewRoomIfAble = (width, height, tiles, maxWidth, maxHeight, fromTile, direction) => {    
    const widthDownRoundedHalf = Math.floor(width/2)
    const widthUpRoundedHalf = Math.ceil(width/2)
    const heightDownRoundedHalf = Math.floor(height/2)
    const heightUpRoundedHalf = Math.ceil(height/2)
    const findTile = (x, y) => tiles.filter(t=>t.x===x&&t.y===y)[0]
    let able = true;
    let offset = {
        x: direction.txt === "top" || direction.txt === "bottom" ? 0 : direction.txt === "left" ? -widthUpRoundedHalf : widthUpRoundedHalf,
        y: direction.txt === "left" || direction.txt === "right" ? 0 : direction.txt === "top" ? -heightUpRoundedHalf : heightUpRoundedHalf
    }
    for (let i=-widthDownRoundedHalf+offset.x-1; i<widthUpRoundedHalf+offset.x+1; i++){
        for (let j=-heightDownRoundedHalf+offset.y-1; j<heightUpRoundedHalf+offset.y+1; j++){
            const tile = findTile(fromTile.x+i, fromTile.y+j);
            if (tile && !(tile.x == fromTile.x && tile.y == fromTile.y)) able = false;            
            if (fromTile.x+i<0 || fromTile.x+i>maxWidth || fromTile.y+j<0 || fromTile.y+j>maxWidth) able = false;
        }
    }    
    if (able) {
        for (let i=-widthDownRoundedHalf+offset.x; i<widthUpRoundedHalf+offset.x; i++){
            for (let j=-heightDownRoundedHalf+offset.y; j<heightUpRoundedHalf+offset.y; j++){
                const newTile = {
                    x: fromTile.x + i,
                    y: fromTile.y + j,
                    type: 'room',
                    topOpen: false,
                    bottomOpen: false,
                    leftOpen: false,
                    rightOpen: false 
                }    
                tiles.push(newTile)                
            }
        }
    }    
}

const step = (tiles, maxWidth, maxHeight, intersectionChance, turnChance, deadEndChance, roomChance) => {        
    const openEndsCount = getOpenEnds(tiles).length;
    const continueFromTile = getRandomOpenEndedTile(tiles);
    if (!continueFromTile) return;
    const continueInDirection = getRandomOpenDirectionOfTile(continueFromTile);    
    if (!continueInDirection) return;
    const growingAction = getGrowingAction(continueFromTile, continueInDirection, intersectionChance, turnChance, deadEndChance, roomChance);        
    if (growingAction === 'room'){
        const min = 3; // must be even
        const max = 11; // must be even
        const randomEvenNumberWidth = Math.floor(Math.random(1) * (max-min)/2)*2+min;
        const randomEvenNumberHeight = Math.floor(Math.random(1) * (max-min)/2)*2+min;
        createNewRoomIfAble(randomEvenNumberWidth, randomEvenNumberHeight, tiles, maxWidth, maxHeight, continueFromTile, continueInDirection)
    } else {        
        createNewTileIfAble(tiles, maxWidth, maxHeight, continueFromTile, continueInDirection, growingAction);
    }
    return openEndsCount;
}

export const generateDungeon = (maxWidth, maxHeight, coreCount, intersectionChance, turnChance, deadEndChance, roomChance) => {
    let tiles = []
    // put down cores
    for (let i=0; i<coreCount; i++){
        tiles.push({
            x: getRandomInt(maxWidth/2)*2,
            y: getRandomInt(maxHeight/2)*2,
            type: 'core',
            topOpen: true,
            bottomOpen: true,
            leftOpen: true,
            rightOpen: true
        })    
    }    
    // 'step'
    let stepCount=0;
    let openEndsCount=4*coreCount;    
    while (openEndsCount>0) {       
        openEndsCount = step(tiles, maxWidth, maxHeight, intersectionChance, turnChance, deadEndChance, roomChance); 
        stepCount++;        
    }
    console.log("generator done: " + stepCount + " steps")
    return tiles;    
}
