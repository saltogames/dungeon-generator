import { useEffect, useRef } from 'react';
import { drawDungeon } from './draw';
import './Screen.css';

// see http://www.petecorey.com/blog/2019/08/19/animating-a-canvas-with-react-hooks/
const getPixelRatio = context => {
  var backingStore =
    context.backingStorePixelRatio ||
    context.webkitBackingStorePixelRatio ||
    context.mozBackingStorePixelRatio ||
    context.msBackingStorePixelRatio ||
    context.oBackingStorePixelRatio ||
    context.backingStorePixelRatio ||
    1;      
  return (window.devicePixelRatio || 1) / backingStore;
};

export const Screen = (props) => {
  const {
    dungeonTiles,
    tileSize
  } = props;
  let canvasRef = useRef();
  useEffect(() => {
      let canvas = canvasRef.current;
      let context = canvas.getContext('2d');
      let ratio = getPixelRatio(context);
      let width = getComputedStyle(canvas)
        .getPropertyValue('width')
        .slice(0, -2);    
      let height = getComputedStyle(canvas)
        .getPropertyValue('height')
        .slice(0, -2);      
      canvas.width = width * ratio;
      canvas.height = height * ratio;
      canvas.style.width = `${width}px`;
      canvas.style.height = `${height}px`;
      drawDungeon(context, dungeonTiles, tileSize)
  });
  return (
    <div className="screen">
      <canvas ref={canvasRef}>    
      </canvas>
    </div>
  )
}
  