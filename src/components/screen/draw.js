const colors = {
    tile: "#323827",
    tileInner: "#1c9040"
}
const drawTile = (context, offsetX, offsetY, size) => {
    context.beginPath();
    context.rect(offsetX, offsetY, size, size);
    context.fillStyle = colors.tile;
    context.fill();
    context.beginPath();
    context.rect(offsetX+1, offsetY+1, size-2, size-2);
    context.fillStyle = colors.tileInner;
    context.fill();
}
export const drawDungeon = (context, tiles, tileSize) => {
    if (!tiles || !tiles.length) return;
    tiles.forEach(tile => drawTile(
        context,
        tile.x * tileSize,
        tile.y * tileSize,
        tileSize
    ));
}